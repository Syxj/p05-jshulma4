//
//  PlayerData.m
//  Incremental Clicker
//
//  Created by Jordan Shulman on 3/20/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerData.h"


@implementation PlayerData

-(id)init
{
    self = [super init];
    if(self)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        //[defaults setBool:NO forKey:@"savedGame"]; //debug code, resets values
        
        if([defaults boolForKey:@"savedGame"])
        {
            _upgrade1 = [defaults floatForKey:@"upgrade1"];
            _upgrade2 = [defaults floatForKey:@"upgrade2"];
            _upgrade3 = [defaults floatForKey:@"upgrade3"];
            
            _upgrade1Cost = [defaults floatForKey:@"upgrade1Cost"];
            _upgrade2Cost = [defaults floatForKey:@"upgrade2Cost"];
            _upgrade3Cost = [defaults floatForKey:@"upgrade3Cost"];
            
            _points = [defaults floatForKey:@"knowledge"];
        }
        
        else
        {
            _upgrade1 = 0;
            _upgrade2 = 0;
            _upgrade3 = 0;
            
            _upgrade1Cost = 100;
            _upgrade2Cost = 500;
            _upgrade3Cost = 1000;
            
            _points = 0;
            
            [defaults setFloat:0 forKey:@"upgrade1"];
            [defaults setFloat:_upgrade1Cost forKey:@"upgrade1Cost"];
            [defaults setFloat:0 forKey:@"upgrade2"];
            [defaults setFloat:_upgrade2Cost forKey:@"upgrade2Cost"];
            [defaults setFloat:0 forKey:@"upgrade3"];
            [defaults setFloat:_upgrade3Cost forKey:@"upgrade3Cost"];
            [defaults setBool:YES forKey:@"savedGame"];
        }
    }
    
    return self;
}

@end

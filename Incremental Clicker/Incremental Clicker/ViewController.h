//
//  ViewController.h
//  Incremental Clicker
//
//  Created by Jordan Shulman on 3/20/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//∂

#import <UIKit/UIKit.h>
#import "GameView.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet GameView *gameView;


@end


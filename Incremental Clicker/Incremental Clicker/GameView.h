//
//  GameView.h
//  Incremental Clicker
//
//  Created by Jordan Shulman on 3/20/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PlayerData.h"

@interface GameView : UIView {}

@property (strong, nonatomic) IBOutlet UILabel *pointsLabel;
@property (strong, nonatomic) IBOutlet UILabel *upgrade1Label;
@property (strong, nonatomic) IBOutlet UILabel *upgrade2Label;
@property (strong, nonatomic) IBOutlet UILabel *upgrade3Label;


@property (strong, nonatomic) IBOutlet UILabel *upgrade1OwnedLabel;
@property (strong, nonatomic) IBOutlet UILabel *upgrade2OwnedLabel;
@property (strong, nonatomic) IBOutlet UILabel *upgrade3OwnedLabel;

@property (strong, nonatomic) IBOutlet UIButton *upgrade1Button;
@property (strong, nonatomic) IBOutlet UIButton *upgrade2Button;
@property (strong, nonatomic) IBOutlet UIButton *upgrade3Button;

@property (strong, nonatomic) PlayerData *pData;

@property (strong, nonatomic) NSUserDefaults *defaults;

-(void)arrange:(CADisplayLink *)sender;

@property (strong, nonatomic) IBOutlet UIImageView *clicker;



@end

//
//  GameView.m
//  Incremental Clicker
//
//  Created by Jordan Shulman on 3/20/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//

#import "GameView.h"

@implementation GameView

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        CGRect bounds = [self bounds];
        
        _defaults = [NSUserDefaults standardUserDefaults];

        _pData = [[PlayerData alloc] init];
        [_upgrade1OwnedLabel setText:[NSString stringWithFormat:@"Owned: %.f", _pData.upgrade1]];
        if(_pData.upgrade1Cost < 1000000) [_upgrade1Label setText:[NSString stringWithFormat:@"Take Notes: %.f knw", _pData.upgrade1Cost]];
        else [_upgrade1Label setText:[NSString stringWithFormat:@"Take Notes: %.f mil knw", (_pData.upgrade1Cost/1000000.0)]];
        
        [_upgrade2OwnedLabel setText:[NSString stringWithFormat:@"Owned: %.f", _pData.upgrade2]];
        if(_pData.upgrade2Cost < 1000000) [_upgrade2Label setText:[NSString stringWithFormat:@"Hire Tutor: %.f knw", _pData.upgrade2Cost]];
        else [_upgrade2Label setText:[NSString stringWithFormat:@"Hire Tutor: %.f mil knw", (_pData.upgrade2Cost/1000000.0)]];
        
        [_upgrade3OwnedLabel setText:[NSString stringWithFormat:@"Owned: %.f", _pData.upgrade3]];
        if(_pData.upgrade3Cost < 1000000) [_upgrade3Label setText:[NSString stringWithFormat:@"Office Hours: %.f knw", _pData.upgrade3Cost]];
        else [_upgrade3Label setText:[NSString stringWithFormat:@"Office Hours: %.f mil knw", (_pData.upgrade3Cost/1000000.0)]];
        
        
        if(_pData.points < 1000000) [_pointsLabel setText:[NSString stringWithFormat:@"Knowledge: %.f", _pData.points]];
        else [_pointsLabel setText:[NSString stringWithFormat:@"Knowledge: %.f mil", (_pData.points/1000000.0)]];

        _clicker = [[UIImageView alloc] initWithFrame:CGRectMake(bounds.size.width/10, bounds.size.height/6, 250, 250)];
        UIImage *bearcat = [UIImage imageNamed:@"bearcat"];
        [_clicker setImage:bearcat];
        //[_clicker setBackgroundColor:[UIColor blackColor]];
        
        //Code for making a UIImageView clickable found on StackOverflow
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickerClicked)];
        singleTap.numberOfTapsRequired = 1;
        [_clicker setUserInteractionEnabled:YES];
        [_clicker addGestureRecognizer:singleTap];
        [self addSubview:_clicker];
    }
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(addPoints) userInfo:nil repeats: YES];

    return self;
}

-(void)clickerClicked
{
    _pData.points++;
}

//Used to update points score automatically
-(void)arrange:(CADisplayLink *)sender
{
    if(_pData.points < 1000000) [_pointsLabel setText:[NSString stringWithFormat:@"Knowledge: %.f", _pData.points]];
    else [_pointsLabel setText:[NSString stringWithFormat:@"Knowledge: %.f mil", (_pData.points/1000000.0)]];
}

-(void)addPoints
{
    _pData.points += (_pData.upgrade1);
    _pData.points += (_pData.upgrade2 * 5);
    _pData.points += (_pData.upgrade3 * 25);
    [_defaults setFloat:_pData.points forKey:@"knowledge"];
}

-(IBAction)upgrade1Buy:(id)sender
{
    if(_pData.points >= _pData.upgrade1Cost)
    {
        _pData.upgrade1++;
        _pData.points -= _pData.upgrade1Cost;
        _pData.upgrade1Cost = (pow(_pData.upgrade1Cost,2.0) + 2*_pData.upgrade1Cost - 1)/100.0;
        [_upgrade1OwnedLabel setText:[NSString stringWithFormat:@"Owned: %.f", _pData.upgrade1]];
        if(_pData.upgrade1Cost < 1000000) [_upgrade1Label setText:[NSString stringWithFormat:@"Take Notes: %.f knw", _pData.upgrade1Cost]];
        else [_upgrade1Label setText:[NSString stringWithFormat:@"Take Notes: %.f mil knw", (_pData.upgrade1Cost/1000000.0)]];
        [_defaults setFloat:_pData.upgrade1 forKey:@"upgrade1"];
        [_defaults setFloat:_pData.upgrade1Cost forKey:@"upgrade1Cost"];
    }
}

-(IBAction)upgrade2Buy:(id)sender
{
    if(_pData.points >= _pData.upgrade2Cost)
    {
        _pData.upgrade2++;
        _pData.points -= _pData.upgrade2Cost;
        _pData.upgrade2Cost = (pow(_pData.upgrade2Cost,2.0) + 2*_pData.upgrade2Cost - 1)/100.0;
        [_upgrade2OwnedLabel setText:[NSString stringWithFormat:@"Owned: %.f", _pData.upgrade2]];
        if(_pData.upgrade2Cost < 1000000) [_upgrade2Label setText:[NSString stringWithFormat:@"Hire Tutor: %.f knw", _pData.upgrade2Cost]];
        else [_upgrade2Label setText:[NSString stringWithFormat:@"Hire Tutor: %.f mil knw", (_pData.upgrade2Cost/1000000.0)]];
        [_defaults setFloat:_pData.upgrade2 forKey:@"upgrade2"];
        [_defaults setFloat:_pData.upgrade2Cost forKey:@"upgrade2Cost"];
    }
}

-(IBAction)upgrade3Buy:(id)sender
{
    if(_pData.points >= _pData.upgrade3Cost)
    {
        _pData.upgrade3++;
        _pData.points -= _pData.upgrade3Cost;
        _pData.upgrade1Cost = (pow(_pData.upgrade3Cost,2.0) + 2*_pData.upgrade3Cost - 1)/100.0;
        [_upgrade3OwnedLabel setText:[NSString stringWithFormat:@"Owned: %.f", _pData.upgrade3]];
        if(_pData.upgrade3Cost < 1000000) [_upgrade3Label setText:[NSString stringWithFormat:@"Office Hours: %.f knw", _pData.upgrade3Cost]];
        else [_upgrade3Label setText:[NSString stringWithFormat:@"Office Hours: %.f mil knw", (_pData.upgrade3Cost/1000000.0)]];
        [_defaults setFloat:_pData.upgrade3 forKey:@"upgrade3"];
        [_defaults setFloat:_pData.upgrade3Cost forKey:@"upgrade3Cost"];
    }
}

@end

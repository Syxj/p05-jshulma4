//
//  PlayerData.h
//  Incremental Clicker
//
//  Created by Jordan Shulman on 3/20/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//

@interface PlayerData : NSObject {}

@property (nonatomic) float upgrade1;
@property (nonatomic) float upgrade1Cost;

@property (nonatomic) float upgrade2;
@property (nonatomic) float upgrade2Cost;

@property (nonatomic) float upgrade3;
@property (nonatomic) float upgrade3Cost;

@property (nonatomic) float points;


@end

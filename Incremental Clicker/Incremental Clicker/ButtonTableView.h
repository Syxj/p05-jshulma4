//
//  ButtonTableView.h
//  Incremental Clicker
//
//  Created by Jordan Shulman on 3/21/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//

@interface ButtonTableView : UITableViewCell {}

@property (strong, nonatomic) IBOutlet UILabel *upgrade1Label;
@property (strong, nonatomic) IBOutlet UILabel *upgrade2Label;
@property (strong, nonatomic) IBOutlet UILabel *upgrade3Label;

@property (strong, nonatomic) IBOutlet UIButton *upgrade1Button;
@property (strong, nonatomic) IBOutlet UIButton *upgrade2Button;
@property (strong, nonatomic) IBOutlet UIButton *upgrade3Button;

@end

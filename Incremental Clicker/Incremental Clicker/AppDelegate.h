//
//  AppDelegate.h
//  Incremental Clicker
//
//  Created by Jordan Shulman on 3/20/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

